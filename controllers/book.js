const e = require("express");
const con = require("../privacy.js");
const csv = require('csvtojson');
const fs = require('fs');
const util = require('util');
var glob = require("glob")
var path = require("path")
const query = util.promisify(con.query).bind(con);

module.exports.top_book = async function (req, res) {
  await query('select book_detail.book_id,title,book_detail.story,rate,book_detail.read_by from book_detail inner join (select book_id,avg(rating) as rate from book_review GROUP by book_id) as review on review.book_id=book_detail.book_id order by book_detail.read_by DESC,rate DESC;',(err,result)=>{
    if(err)
    console.log(err);
    else{
      res.status(200).json(result);
    }
  });
  
}

module.exports.new_book = async function (req, res) {
    let book=await query('select book_id,title,story,added_on from book_detail order by added_on DESC limit 15');
    res.status(200).json({msg:"latest 15 book",result:book});
}

module.exports.upload = async (req, res) => {
    console.log(req)
    var filepath = path.join(__dirname,'..', req.file.path);
    // filepath += '/uploads/*'
    console.log(filepath);
  
    csv().fromFile(filepath).then(async (obj) => {
        let values=[]
        for (let i = 0; i < obj.length; i++) {
            values.push([obj[i].title, obj[i].story,new Date(),0,req.user.id])
          }
          await query('insert into book_detail(title,story,added_on,read_by,user_id) values?',[values],(err,result)=>{
              console.log('succesfully entered value in sql');
          });
        console.log(values);
    });
   
    
    glob(filepath, function (er, files) {
      files.forEach(function (pat) {
        console.log('f1=', pat);
        try {
          fs.unlinkSync(pat);
        }
        catch (e) {
          console.log(e);
        }
      })
    });
    // console.log(file);
    // for(var name in file)
    // { console.log(name)
    //   fs.unlink(name);}
    res.status(200).json({msg:"book uploaded"});
  
  }

  