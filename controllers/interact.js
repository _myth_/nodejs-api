const alert = require('alert');
const con = require("../privacy.js");
var path = require("path")
const csv = require('csvtojson');
var glob = require("glob")
const fs = require('fs');
const util = require('util');
const Json2csvParser = require("json2csv").Parser;
const { resolveSoa } = require('dns');
const query = util.promisify(con.query).bind(con);

module.exports.show_book = async function (req, res) {
      let book=await query('select book_id,title,story from book_detail');
      console.log(book);
      res.render('read_book.ejs',{result:book});
  }
module.exports.read = async (req, res) => {

      console.log('body', req.body);
      if(!req.body.first_name)
      {
            req.body.first_name=req.user.first_name;
      }
      await query(`update table book_detail set read_by=read_by+1 where id=${req.body.book_id}`, (error, result) => {
            if (error)
                  console.log(error);
            
                  res.status(200).json({msg: ` book with id=${req.body.book_id} is read by user=${req.body.first_name}`});
            
      });

}

module.exports.rateit = async (req, res) => {

      console.log('body', req.body, req.user);
      if(!req.body.id)
      {
            req.body.id=req.user.id;
      }
      var qry = `insert into book_review values(${parseInt(req.body.book_id)},${parseInt(req.body.id)},${parseInt(req.body.rating)})`;
      await query(qry, (err, result) => {
            if (err)
             {
                        res.status(200).json({msg: 'already reated this book'});
            }
            res.status(200).json({msg:`rate book with id=${req.body.book_id}`});

      });
      res.redirect('/');

}
module.exports.read_rate = async (req, res) => {

      console.log('body', req.body, req.user);
      
      await query(`select book_id,title,story from book_detail`, (err, result) => {
           if(err)
           console.log(err);

           
                  res.render('rate_book.ejs',{result:result});

      });
      res.redirect('/');

}
