# About
A basic project to demonstrate the making of APIs to fetch data and updating values to database . And reflect the usage of docker.

### RUN:
* Clone the folder from gitlab :git clone  https://gitlab.com/_myth_/nodejs-api.git
* Run `npm i`  to install all the dependencies inside the folder
* Enter the above database configuration in privacy.js file.
* Start on localhost : npm start


## RUN USING DOCKER:
* Install docker .
* Pull docker image : docker pull thrylos/nodejs-api
* Run docker container : docker run -it -p [port]:5000 thrylos/nodejs-api:latest
* Put the port on which you want to run on your local host,5000 port is expose for docker image.
* Open localhost:[port] to use.   
