const con = require("../privacy.js");
const passport = require('passport');

const LocalStrategy = require('passport-local').Strategy;
console.log('Inside passport_local strategy verify');
// authentication using passport
passport.use(new LocalStrategy(
    {
        usernameField: 'id',
        passReqToCallback: true
    },
   async function (req, email, phone, done) {
        console.log("checkig user",req.body);
        // find a user and establish the identity
        // id = id.trim();
        

        // table = table.trim();
        try {
            
                await con.query(`select * from user where email_id=? and phone_no=?`, [email, phone], (err, user) => {
                    if (err) {
                        //req.flash('error', err);
                        console.log(err);
                        return done(null,false);
                    }
                    console.log(user[0]);
                    if (!user[0]) {
                        console.log('error', 'Invalid Username/Password');
                        
                        return done(null, false);
                    }
                    
                    console.log(`${user[0].id} signed in!`);
                    user[0].login = req.body.login;
                    req.session.type = req.body.login;
                    return done(null, user[0]);
                });
            
        }
        catch {
            console.log('canot find');
            return done(null, false);
        }

    }
));

// serializing the user to decide which key is to be kept in the cookies
passport.serializeUser(function (user, done) {
    console.log('inside serialize');
    done(null, user);
});

// deserializing the user from the key in the cookies
passport.deserializeUser(function (user, done) {
    console.log('inside deserialize and login=>', user.login, ' id=>', user);
    con.query(`select * from user where email_id='${user.email_id}'`, (err, users) => {
        //return done(null, user);
        if (err) {
           
            return done(err);
        }
        else if(!users){
                console.log('Error in finding user --> Passport');
                return done(null,false);
            }
        // console.log(users, user);
        return done(null, users[0]);
    });
});

// check if the user is authenticated
passport.checkAuthentication = function (req, res, next) {
    // if the user is signed in, then pass on the request to the next function(controller's action)
    console.log("i''m inside checkauth =>");
    if (req.isAuthenticated()) {
        console.log('user found in checkauth');
        console.log(req.body, req.user);
        return next();
    }
    console.log("user not signed in");
    // if the user is not signed in
    console.log('check type of login=>', req.body ,req.session);
    return res.render('login.ejs');
}

passport.setAuthenticatedUser = function (req, res, next) {
    console.log("inside setauth func");
    if (req.isAuthenticated()) {
        console.log('saving user info in setauth');
        // req.user contains the current signed in user from the session cookie and we are just sending it to the local for the views
        res.locals.user = req.user;
    }
    next();
}

module.exports = passport;