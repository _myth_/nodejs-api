const express = require('express');
const passport = require('passport');
const fs = require('fs');

const router = express.Router();
var multer = require('multer');
const upload = (multer({ dest: 'uploads/' }));
router.get('/', passport.checkAuthentication, (req, res) => {
  console.log("enter admin login")
  res.redirect('/login');
});
const passp = require('../controllers/auth');
router.use('/login', require('./users'));
router.use('/book',require('./book'));
router.use('/user_interact',require('./user_interact'));
router.use('/update_user',require('./update_user'));


router.get('/logout', passport.checkAuthentication, (req, res) => {
  req.logout();
  req.session.uid = undefined;
  req.session.user_position = undefined;
  req.flash("success", "successfully logout!!");
  res.locals.message=req.flash("success");
  res.render('login.ejs');

});

router.get('/*',(req,res)=>{
  res.render("pagenotfound.ejs");
});
module.exports = router;