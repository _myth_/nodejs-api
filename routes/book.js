const express = require('express');
const passport = require('../controllers/auth');
var multer = require('multer');
const upload = (multer({ dest: 'uploads/' }));
const router = express.Router();
const book = require('../controllers/book');
const type = require('../controllers/profile');
router.get('/',passport.checkAuthenticated, (req, res, next) => {
    res.render('upload.ejs');
  });
router.post("/upload_book", passport.checkAuthenticated,upload.single('avatar'),book.upload);
router.get("/new_book",book.new_book);
router.get("/top_book",book.top_book);
module.exports = router;