const express = require('express');
const router = express.Router();
const prof=require('../controllers/profile')
router.get('/', (req,res)=>{
    res.render('profile.ejs');
});

router.post("/profile",prof.update);
module.exports = router;