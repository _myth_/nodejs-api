const express = require('express');
const passport = require('../controllers/auth');
const router = express.Router();
const table=require('../controllers/interact')
router.post("/read",passport.checkAuthenticated,table.read);
router.get('/showbook',passport.checkAuthenticated, table.show_book);
router.post("/rateit",passport.checkAuthenticated,table.rateit);
router.get("/rateit",passport.checkAuthenticated,table.read_rate);
module.exports = router;